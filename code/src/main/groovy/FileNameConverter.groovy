class FileNameConverter {


    List<File> getAllFilesAsList(String dirPath) {
        File imageDir = new File(dirPath)
        imageDir.listFiles() as List
    }

    void renameFiles(List<File> fileList) {
        fileList.each { file ->
            def (_, supplierModelNameAndNumber, dummy, id, count, extension) = (file.name =~ /(.*%.*%.*%)(.*%)(.*%)\s?\(?(\d+)?\)?(\.\w*)/)[0]
            file.renameTo("${file.parent}/$id${calcCount(count)}%$supplierModelNameAndNumber$extension")
        }
    }

    String calcCount(String count) {
        if(count){
            if(count.size() <2)
                return "0$count"
            else
                return count

        }
        else
            return '00'
    }
}
