/*
To run this script run the following and don't forget to define the path property.
You can set the path either relative to your script file or absulute.

groovy -Dpath="<set your path here, this is not a valid path>" code/src/main/groovy/rename.groovy

 */

List<File> getAllFilesAsList(String dirPath) {
    File imageDir = new File(dirPath)
    imageDir.listFiles() as List
}

void renameFiles(List<File> fileList) {
    fileList.each { file ->
        def (_, supplierModelNameAndNumber, dummy, id, count, extension) = (file.name =~ /(.*%.*%.*%)(.*%)(.*%)\s?\(?(\d+)?\)?(\.\w*)/)[0]
        file.renameTo("${file.parent}/$id${calcCount(count)}%$supplierModelNameAndNumber$extension")
    }
}

String calcCount(String count) {
    if (count) {
        if (count.size() < 2)
            return "0$count"
        else
            return count

    } else
        return '00'
}

renameFiles(getAllFilesAsList(System.getProperty('path')))
