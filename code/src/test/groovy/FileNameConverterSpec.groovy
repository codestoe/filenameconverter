import spock.lang.Specification

class FileNameConverterSpec extends Specification{


    public static final String TEST_DIRECTORY = "src/test/resources"

    def "should read all files" () {
        given:
        FileNameConverter converter = new FileNameConverter()
        when:
        def fileList = converter.getAllFilesAsList(TEST_DIRECTORY)

        then:
        fileList.size() == 6
    }

    def "should set id as prefix and remove as suffix" () {
        given:
        FileNameConverter converter = new FileNameConverter()
        List<File> fileList = converter.getAllFilesAsList(TEST_DIRECTORY)
        List<String> oldNames = fileList.path

        when:
        converter.renameFiles(fileList)

        then:
        converter.getAllFilesAsList(TEST_DIRECTORY).name.sort() == expectedFileNames

        cleanup:
        converter.getAllFilesAsList(TEST_DIRECTORY).eachWithIndex{ File file, int index ->
            file.renameTo(oldNames[index])
        }

        where:
        expectedFileNames = [
                '1929%00%Anschutz%Miroku%1270%.JPG',
                '1929%01%Anschutz%Miroku%1270%.JPG',
                '1929%02%Anschutz%Miroku%1270%.JPG',
                '1929%03%Anschutz%Miroku%1270%.JPG',
                '1929%04%Anschutz%Miroku%1270%.JPG',
                '1929%05%Anschutz%Miroku%1270%.JPG'
        ]
    }







}
